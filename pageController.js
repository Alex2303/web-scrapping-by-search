const pageScraper = require('./pageScraper');

async function scraperController(browserInstance, ref){
    let browser;
    try{
        browser = await browserInstance;
        await pageScraper.scraper(browser, ref);
    }
    catch(err){
        console.log("Could not resolve the browser instance => ", err);
    }
}

module.exports = (browserInstance, ref) => scraperController(browserInstance, ref)
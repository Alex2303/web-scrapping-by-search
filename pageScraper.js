const ObjectsToCsv = require('objects-to-csv');
const userInfos = require('./config');
var user = userInfos.user;
var password = userInfos.pwd;

const scraperObject = {
    url: 'https://polspotten.nl/en/professional-buyers-login',
    async scraper(browser, ref) {
        let page = await browser.newPage();
        console.log(`Navigating to ${this.url}...`);
        await page.goto(this.url);
        async function scrapeCurrentPage(ref){
            // On se connecte et on navigue vers la page des produits
            let username = await page.$x('/html/body/div/div[3]/div[2]/div/div[2]/div/div[1]/div/div/form/div[1]/div[1]/div/input');
            await username[0].type(user);
            let pwd = await page.$x('/html/body/div/div[3]/div[2]/div/div[2]/div/div[1]/div/div/form/div[1]/div[2]/div/input');
            await pwd[0].type(password);
            await page.keyboard.press('Enter');
            var arr = ref.split(",");
            for(let el of arr){
                let newPage = await browser.newPage();
                await newPage.goto('https://polspotten.nl/products/all');
                await newPage.waitFor(3000);
                let input = await newPage.$x('/html/body/div[1]/div[3]/div[3]/div[5]/div/input');
                await input[0].type(el);
                await newPage.waitForNavigation();
                await newPage.waitFor(3000);
                await Promise.all([
                    newPage.waitForNavigation({waitUntil: 'load'}),
                    newPage.$eval('.products-content a', el => el.click())
                ]);
                await newPage.waitFor(3000);
                let dataObj = {};
                dataObj['product_name'] = await newPage.$eval('.product-info-panel > div > div > h1', text => text.textContent.replace(/(\r\n\t|\n|\r|\t)/gm, "")); //removes all three types of line breaks                
                if(await newPage.$('.product-info-panel > div > div >.prices > .discounted') !== null ){
                    dataObj['discount_price'] = await newPage.$eval('.product-info-panel > div > div >.prices > .discounted', text => text.textContent.replace(/[^0-9\.-]+/g,""));
                    dataObj['new_price'] = await newPage.$eval('.product-info-panel > div > div >.prices > .new-price', text => text.textContent.replace(/[^0-9\.-]+/g,""));
                } 
                else {
                    dataObj['cost_price'] = await newPage.$eval('.product-info-panel > div > div >.prices > p:nth-child(1)', text => text.textContent.replace(/[^0-9\.-]+/g,""));
                }
                let product_reference = await newPage.$eval('.product-info-panel > div > p', text => text.textContent.replace(/(\r\n\t|\n|\r|\t)/gm, ""));
                dataObj['product_reference'] = product_reference.split(': ')[1];
                dataObj['product_quantity'] = await newPage.$eval('.quantity-selector > .amt', text => text.textContent.replace(/(\r\n\t|\n|\r|\t)/gm, ""));
                console.log(dataObj);
                // await browser.close();
                let data = [dataObj];
                (async () => {
                    const csv = new ObjectsToCsv(data); 
                    await csv.toDisk('./products.csv', { append: true });
                    console.log(await csv.toString());
                })();
            }
        }
        await scrapeCurrentPage(ref);
    }
}

module.exports = scraperObject;

/*
Global pattern flags :

    g modifier: global. All matches (don't return after first match)
    m modifier: multi line. Causes ^ and $ to match the begin/end of each line (not only begin/end of string)

function explanation :
    \n carriage return
    \r line-feed (newline)
    \t tab

*/
var sha1 = require('sha1');

var connection = require('./connection');

const browserObject = require('./browser');
const scraperController = require('./pageController');

let browserInstance = browserObject.startBrowser();

var express = require('express');
var app = express();
const bodyParser = require('body-parser');
var session = require('express-session');

app.use(express.static(__dirname + '/public'));

app.use(session({
	secret: 'secret',
	resave: true,
	saveUninitialized: true
}));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.get('/', function(req, res) {
    res.render('login.ejs');
})

app.post('/login', function(req, res) {
    var username = req.body.username;
    var password = req.body.password;
    if(username && password){
        connection.query('SELECT * FROM admin WHERE username = ? AND password = ?', [username, sha1(password)], function(error, results, fields){
            if(results.length > 0) {
                req.session.loggedin = true;
                req.session.username = username;
                res.redirect('/protected_page');
            } else {
                res.send('Identifiant et/ou mot de passe incorrect!');
            } 
            res.end(); //end the response process
        })
    } else {
        res.send('Merci de renseigner l\'identifiant et le mot de passe !');
        res.end();
    }
})

app.get('/protected_page' ,function(req, res) {
    if(req.session.loggedin){
        res.render('scraper.ejs', {user: req.session.username});
    } else {
        res.redirect('/login');
    }
    res.end();
});

app.post('/sendData', function(req, res){
    const ref = req.body.ref;
    scraperController(browserInstance, ref);
})

app.listen(8080);


